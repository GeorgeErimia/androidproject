package com.example.myapplication.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myapplication.fragments.FragmentSignIn
import com.example.myapplication.R

class SignInSignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in_sign_up)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(
            R.id.fl_container,
            FragmentSignIn(), "FragmentSignIn")
        transaction.commit()
    }
}
