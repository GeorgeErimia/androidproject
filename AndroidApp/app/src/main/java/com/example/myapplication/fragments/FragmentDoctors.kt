package com.example.myapplication.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.myapplication.adapters.DoctorsAdapter
import com.example.myapplication.adapters.OnDoctorItemClickListener

import com.example.myapplication.R
import com.example.myapplication.room.AppDatabase
import com.example.myapplication.room.Doctor
import kotlinx.android.synthetic.main.fragment_doctors.*

class FragmentDoctors : Fragment(),
    OnDoctorItemClickListener {

    var doctorArrayList: ArrayList<Doctor> = ArrayList()
    lateinit var doctorList: List<Doctor>

    lateinit var preference: SharedPreferences
    val pref_show_data = "Data"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_doctors, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        AppDatabase.getAppDatabase(activity!!)

        recycler_view_doctors.layoutManager = GridLayoutManager(activity, 1)

        preference = activity!!.getSharedPreferences("DataDoctors", Context.MODE_PRIVATE)
        if (preference.getBoolean(pref_show_data, true)) {
            val listOfDoctors = ArrayList<Doctor>()
            listOfDoctors.add(
                Doctor(
                    1,
                    "George",
                    "Erimia",
                    "Cardiologist",
                    "Good Doctor",
                    "georgeerimia@gmail.com",
                    "https://e7.pngegg.com/pngimages/37/664/png-clipart-doctors-and-nurses-doctors-and-nurses-thumbnail.png"
                )
            )
            listOfDoctors.add(
                Doctor(
                    2,
                    "Claudiu",
                    "Dragoiu",
                    "Neurologist",
                    "Good Doctor",
                    "claudiudragoiu@gmail.com",
                    "https://i.imgur.com/Z3RFDn4.jpg"
                )
            )
            listOfDoctors.add(
                Doctor(
                    3,
                    "Mihai",
                    "Anghel",
                    "Psychiatry",
                    "Good Doctor",
                    "anghelmihai2000@gmail.com",
                    "https://i.imgur.com/Ak7pdPB.png"
                )
            )
            listOfDoctors.add(
                Doctor(
                    4,
                    "Gabriel",
                    "Iglesias",
                    "Obstetrics and gynecology",
                    "Good Doctor",
                    "alexandru991999@yahoo.com",
                    "https://i.imgur.com/BCD4IQA.jpg"
                )
            )
            listOfDoctors.add(
                Doctor(
                    5,
                    "Adrian",
                    "Matei",
                    "Dermatologist",
                    "Good Doctor",
                    "matei.adrian@gmail.com",
                    "https://i.imgur.com/GbSsT43.png"
                )
            )
            for (doctor in listOfDoctors) {
                addDoctorsToDB(doctor)
            }
        }

        insertDoctorsInRecyclerViewFromDB()
    }

    private fun addDoctorsToDB(doctor: Doctor) {

        class InsertDoctorToDB : AsyncTask<Void, Void, Void>() {
            override fun doInBackground(vararg params: Void?): Void? {
                AppDatabase.getAppDatabase(activity!!).doctorDao().insert(doctor)

                val editor = preference.edit()
                editor.putBoolean(pref_show_data, false)
                editor.apply()
                return null
            }
        }

        InsertDoctorToDB().execute()
    }

    private fun insertDoctorsInRecyclerViewFromDB() {

        class GetDoctors : AsyncTask<Void, Void, Void>() {

            override fun doInBackground(vararg params: Void?): Void? {
                doctorList = AppDatabase.getAppDatabase(activity!!).doctorDao().getAll()
                doctorArrayList.clear()
                doctorArrayList.addAll(doctorList)
                activity!!.runOnUiThread {
                    recycler_view_doctors.adapter =
                        DoctorsAdapter(
                            doctorArrayList,
                            this@FragmentDoctors
                        )
                }
                return null
            }
        }
        GetDoctors().execute()
    }

    private fun sendEmail(recipient: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.data = Uri.parse("mailto:")
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        val subject = "Medical Examination"
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)

        try {
            startActivity(Intent.createChooser(intent, "Choose Email Client..."))
        } catch (e: Exception) {
            Toast.makeText(activity, e.message, Toast.LENGTH_SHORT).show()
        }

    }

    override fun onItemClick(doctor: Doctor, position: Int) {
        sendEmail(doctor.email)
    }

}
