package com.example.myapplication.models

class Appointment (
    var id : String = "",
    var patientId : String = "",
    var doctor : String = "",
    var description : String = "",
    var date : String = "",
    var hour : String = ""
)
{

}